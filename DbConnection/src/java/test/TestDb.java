package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TestDb {

    static Integer ultimoRegistro = 0;

    public static void main(String[] args) {
        System.out.println("[...] main");
        consultar();
        insertar();
        consultar();
        borrar();
        consultar();
        System.out.println("[OK] main");
    }

    public static void borrar() {
        System.out.println("[...] borrar");
        Connection con = null;
        String usuario = "cine";
        String clave = "cine";
        String host = "jdbc:mysql://localhost:3306/nataliaCine";
        String consultaSql = "DELETE FROM candy WHERE candy_id = ?;";
        try {
            con = DriverManager.getConnection(host, usuario, clave);
            PreparedStatement pstm = con.prepareStatement(consultaSql);
            pstm.setInt(1, ultimoRegistro);
            pstm.execute();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println("[OK] borrar");
    }

    public static void insertar() {
        System.out.println("[...] insertar");
        Connection con = null;
        String usuario = "cine";
        String clave = "cine";
        String host = "jdbc:mysql://localhost:3306/nataliaCine";
        String consultaSql = "INSERT INTO candy(candy_nombr, candy_precio) VALUES(?,?);";
        String producto = "Dona";
        String precio = "200";

        try {
            con = DriverManager.getConnection(host, usuario, clave);
            PreparedStatement pstm = con.prepareStatement(consultaSql, Statement.RETURN_GENERATED_KEYS);
            pstm.setString(1, producto);
            pstm.setString(2, precio);
            pstm.execute();

            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                ultimoRegistro = rs.getInt(1);
            }
            System.out.println("Numero de registro insertado: " + ultimoRegistro);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        System.out.println("[OK] insertar");
    }

    public static void consultar() {
        System.out.println("[...] consultar");

        Connection con = null;
        String usuario = "cine";
        String clave = "cine";
        String host = "jdbc:mysql://localhost:3306/nataliaCine";
        String consultaSql = "SELECT * FROM candy;";
        try {
            con = DriverManager.getConnection(host, usuario, clave);
            PreparedStatement pstm = con.prepareStatement(consultaSql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                System.out.println("Nombre: " + rs.getString("candy_nombr") + "Precio: " + rs.getString("candy_precio"));
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }

        System.out.println("[OK] consultar");
    }
}
